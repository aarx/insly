<?php
$fullName = 'Argo Käsper';

typePrint($fullName, 100);

function typePrint(string $phrase, int $typeSpeed = 1000) {
    for ($i = 0; $i < strlen($phrase); $i++) {
        echo $phrase[$i];
        usleep($typeSpeed * 1000);
    }
}
?>