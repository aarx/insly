<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Calculator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="scripts.js"></script>
    <style rel="stylesheet">
        .repayments-table {
            margin-top: 2rem;
        }

        .hidden {
            display: none;
        }
    </style>
</head>
<body>
    <div class="container">
        <form onsubmit="submitForm(event)">
            <div class="form-group">
                <label for="estValue">Estimated Value</label>
                <input type="number" class="form-control" name="estValue" placeholder="Enter value" required step="0.01" min="0">
            </div>
            <div class="form-group">
                <label for="tax">Tax Percentage</label>
                <input type="number" class="form-control" name="tax" placeholder="0 - 100%" required step="0.01" min="0" max="100">
            </div>
            <div class="form-group">
                <label for="instalments">Number of Instalments</label>
                <input type="number" class="form-control" name="instalments"  placeholder="1 - 12" required step="1" min="1" max="12">
            </div>
            <button type="submit" class="btn btn-primary">Calculate</button>
            <button type="reset" class="btn btn-secondary" onclick="resetForm()">Reset</button>
        </form>

        <div class="repayments-table hidden">
            <table class="table">
                <thead>
                    <tr id="instalments">
                        <th></th>
                        <th>Policy</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="value">
                        <td>Value</td>
                    </tr>
                    <tr id="premium">
                        <td>Base Premium <span>(11%)</span></td>
                    </tr>
                    <tr id="comission">
                        <td>Comission <span>(17%)</span></td>
                    </tr>
                    <tr id="tax">
                        <td>Tax <span>(10%)</span></td>
                    </tr>
                    <tr id="total">
                        <td><strong>Total cost</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</body>
</html>