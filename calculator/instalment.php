<?php
namespace calculator;

class Instalment {

    public $number;
    public $base = '0';
    public $comission = '0';
    public $tax = '0';
    public $total = '0';

    public function __construct(int $number, string $base, string $comission, string $tax) {
        $this->number = $number;
        $this->base = $base;
        $this->comission = $comission;
        $this->tax = $tax;
        $this->total = bcadd(bcadd($base, $comission, 2), $tax, 2);
    }
}

?>