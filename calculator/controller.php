<?php

$requestBody = json_decode(file_get_contents('php://input'), true);

if (!isset($requestBody['action'])) {
    echo response(['error' => 'No valid action is given'], 412);
    die();
}
$action = $requestBody['action'];

if ($action == 'calculate') {
    $formData = $requestBody['data'];
    // Validation
    if (isset($formData)) {
        if (!isset($formData['value'])) {
            $errors[] = ['value' => 'value is required'];
        } 

        if (!isset($formData['tax'])) {
            $errors[] = ['tax' => 'tax is required'];
        } else {
            if ($formData['tax'] < 0 || $formData['tax'] > 100) {
                $errors[] = ['tax' => 'tax must be between 0 and 100'];
            }
        }

        if (!isset($formData['instalments'])) {
            $errors[] = ['instalments' => 'instalments is required'];
        } else {
            if ($formData['instalments'] < 1 || $formData['instalments'] > 12) {
                $errors[] = ['instalments' => 'instalments must be between 1 and 12'];
            }
        }

        if (empty($errors)) {
            bootClasses();
            
            $calculator = new calculator\Calculator($formData['value'], $formData['tax'], $formData['instalments']);
            $repaymentTable = $calculator->calculate();
            
            echo response($repaymentTable);
        } else {
            echo response($errors, 412); 
        }
    
        
    } else {
        echo response(['error' => 'No data sent'], 412);
    }
    
} else {
    echo response(['error' => 'Page not found'], 404);
}

function response(array $data = [], int $statusCode = 200) {
    http_response_code($statusCode);
    return json_encode($data);
}

// Helper function to boot all required classes
function bootClasses() {
    require_once(__DIR__ . '/calculator.php');
    require_once(__DIR__ . '/instalment.php');
}
?>