<?php
namespace calculator;

use calculator\Instalment;

class Calculator {

    protected $basePercentage = 11;

    protected $excemptPercentage = 13;
    protected $excemptDay = 5; // Friday
    protected $excemptStartHours = 15;
    protected $excemptEndHours = 20;

    protected $comissionFee = 17;

    public $instalments = [];

    public $noOfInstalments;
    public $basePrice;
    public $tax;

    public function __construct(string $basePrice, string $tax, string $instalments, $requestedAt = null) {
        $this->setInitialValues($basePrice, $tax, $instalments);
    }

    public function calculate() {
        $instalments = [];

        $basePremium = bcmul($this->basePrice, bcdiv($this->basePercentage, 100, 2), 2);
        $instalmentPremium = bcdiv($basePremium, $this->noOfInstalments, 2);

        for ($i = 0; $i < $this->noOfInstalments; $i++) {
            $comission = bcmul($instalmentPremium, bcdiv($this->comissionFee, 100, 2), 2);
            $tax = bcmul($instalmentPremium, bcdiv($this->tax, 100, 2), 2);
            $instalments[] = new Instalment($i+1, $instalmentPremium, $comission, $tax);
        }
        $this->instalments = $instalments;
        $totals = $this->getTotals();
        return $this->formatResponseMatrix($instalments, $totals);
    }

    private function setInitialValues(string $basePrice, string $tax, string $instalments) {
        $this->basePrice = $basePrice;
        $this->tax = $tax;
        $this->noOfInstalments = $instalments;
    }

    private function getTotals() {
        $basePremium = '0';
        $comission = '0';
        $tax = '0';
        $total = '0';
        foreach ($this->instalments as $instalment) {
            $basePremium = bcadd($basePremium, $instalment->base, 2);
            $comission = bcadd($comission, $instalment->comission, 2);
            $tax = bcadd($tax, $instalment->tax, 2);
            $total = bcadd($total, $instalment->total, 2);
        } 

        return [
            'basePremium' => $basePremium,
            'comission' => $comission,
            'tax' => $tax,
            'total' => $total,
        ];
    }

    private function formatResponseMatrix(array $instalments, array $totals = []) {
        $response = [
            'value' => number_format($this->basePrice, 2),
            'premium' => $this->basePercentage,
            'comission' => $this->comissionFee,
            'tax' => $this->tax,
            'instalments' => [],
            'totals' => $totals
        ];
        foreach ($instalments as $instalment) {
            $response['instalments'][] = $instalment;
        }

        return $response;
    }
}
?>