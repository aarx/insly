
function submitForm(event) {
    event.preventDefault();
    
    var formElements = event.target.elements;
    var formData = {
        'data' : {
            'value': formElements[0].value,
            'tax': formElements[1].value,
            'instalments': formElements[2].value
        },
        'action': 'calculate'
    };

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var data = JSON.parse(xhr.responseText);

            if (!data) {
                return false;
            }
            // clear previously added table cells
            var removable = document.getElementsByClassName('added-from-server');
            while(removable[0]) {
                removable[0].parentNode.removeChild(removable[0]);
            }

            var totals = data.totals;
            var instalments = data.instalments;

            // add base value into the table
            var value = document.createElement('td');
            value.innerHTML = data.value;
            value.className = 'added-from-server';
            document.getElementById('value').appendChild(value);

            // add correct percentages
            document.getElementById('premium').children[0]
                .getElementsByTagName('span')[0].innerHTML = '(' + data.premium + '%)';
            document.getElementById('comission').children[0]
                .getElementsByTagName('span')[0].innerHTML = '(' + data.comission + '%)';
            document.getElementById('tax').children[0]
                .getElementsByTagName('span')[0].innerHTML = '(' + data.tax + '%)';

            // add totals into the table
            var premium = document.createElement('td');
            premium.innerHTML = totals.basePremium;
            premium.className = 'added-from-server';
            document.getElementById('premium').appendChild(premium)
            
            var comission = document.createElement('td');
            comission.innerHTML = totals.comission;
            comission.className = 'added-from-server';
            document.getElementById('comission').appendChild(comission)
            
            var tax = document.createElement('td');
            tax.innerHTML = totals.tax;
            tax.className = 'added-from-server';
            document.getElementById('tax').appendChild(tax)
            
            var total = document.createElement('td');
            total.innerHTML = '<strong>' + totals.total + '</strong>';
            total.className = 'added-from-server';
            document.getElementById('total').appendChild(total)

            // add instalments into the table
            for (var instalment of instalments) {
                var instalmentNumber = document.createElement('th');
                instalmentNumber.innerHTML = instalment.number + ' instalment';
                instalmentNumber.className = 'added-from-server';
                document.getElementById('instalments').appendChild(instalmentNumber);

                premium = document.createElement('td');
                premium.innerHTML = instalment.base;
                premium.className = 'added-from-server';
                document.getElementById('premium').appendChild(premium)
                
                comission = document.createElement('td');
                comission.innerHTML = instalment.comission;
                comission.className = 'added-from-server';
                document.getElementById('comission').appendChild(comission)
                
                tax = document.createElement('td');
                tax.innerHTML = instalment.tax;
                tax.className = 'added-from-server';
                document.getElementById('tax').appendChild(tax)
                
                total = document.createElement('td');
                total.innerHTML = instalment.total;
                total.className = 'added-from-server';
                document.getElementById('total').appendChild(total)
            }

            document.getElementsByClassName('repayments-table')[0].classList.remove('hidden'); // show table

        }
    };

    xhr.open('POST', 'controller.php', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(formData));

}

function resetForm() {
    document.getElementsByClassName('repayments-table')[0].classList.add('hidden'); // hide table

    // clear previously added table cells
    var removable = document.getElementsByClassName('added-from-server');
    while(removable[0]) {
        removable[0].parentNode.removeChild(removable[0]);
    }
}