CREATE TABLE employee (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    ssn VARCHAR(15) NULL,
    is_employed TINYINT NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    phone VARCHAR(20) NULL,
    address VARCHAR(255) NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

CREATE TABLE log (
    id BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    employee_id INT(6) UNSIGNED NOT NULL,
    action VARCHAR(15) NULL,
    entry_type VARCHAR(255) NOT NULL,
    entry_id INT(11) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE introduction (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    employee_id INT(6) UNSIGNED NOT NULL,
    value TEXT NULL,
    lang_code VARCHAR(6) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

CREATE TABLE education (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    employee_id INT(6) UNSIGNED NOT NULL,
    school_name VARCHAR(255) NOT NULL,
    start_date DATE NULL,
    end_date DATE NULL,
    speciality VARCHAR(255) NULL,
    comments TEXT NULL,
    lang_code VARCHAR(6) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

CREATE TABLE work_experience (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    employee_id INT(6) UNSIGNED NOT NULL,
    school_name VARCHAR(255) NOT NULL,
    start_date DATE NULL,
    end_date DATE NULL,
    occupation VARCHAR(255) NULL,
    comments TEXT NULL,
    lang_code VARCHAR(6) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

CREATE TABLE language (
    code VARCHAR(6) PRIMARY KEY,
    value VARCHAR(20) NOT NULL,
    description VARCHAR(255) NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

ALTER TABLE log ADD CONSTRAINT fk_log_employee_id FOREIGN KEY (employee_id) REFERENCES employee(id);

ALTER TABLE introduction ADD CONSTRAINT fk_introduction_employee_id FOREIGN KEY (employee_id) REFERENCES employee(id), ADD CONSTRAINT fk_introduction_lang_code FOREIGN KEY (lang_code) REFERENCES language(code);

ALTER TABLE education ADD CONSTRAINT fk_education_employee_id FOREIGN KEY (employee_id) REFERENCES employee(id), ADD CONSTRAINT fk_education_lang_code FOREIGN KEY (lang_code) REFERENCES language(code);

ALTER TABLE work_experience ADD CONSTRAINT fk_w_e_employee_id FOREIGN KEY (employee_id) REFERENCES employee(id), ADD CONSTRAINT fk_w_e_lang_code FOREIGN KEY (lang_code) REFERENCES language(code);

/* INSERT DATA */
INSERT INTO language (code, value) VALUES ('eng', 'english');
INSERT INTO language (code, value) VALUES ('es', 'spanish');
INSERT INTO language (code, value) VALUES ('fr', 'french');

INSERT INTO employee (name, ssn, is_employed, email, phone, address) VALUES ('Test Worker', '1234567890', '1', 'test.worker@test.ee', '01234567', 'Test St 20, Testland 1122');

INSERT INTO education (employee_id, school_name, start_date, end_date, comments, lang_code) VALUES ('1', 'Test School', '2000-09-01', '2012-06-20', 'This is my school', 'eng');
INSERT INTO education (employee_id, school_name, start_date, end_date, comments, lang_code) VALUES ('1', 'L\'ecole de Teste', '2000-09-01', '2012-06-20', 'Tres bien', 'fr');

INSERT INTO introduction (employee_id, value, lang_code) VALUES ('1', 'This is my introduction', 'eng');
INSERT INTO introduction (employee_id, value, lang_code) VALUES ('1', 'Il est moi intro', 'fr');

/* SELECT ONE EMPLOYEE */
SELECT employee.*, introduction.value as introduction, education.school_name, education.lang_code FROM employee 
	LEFT JOIN introduction ON employee.id = introduction.employee_id
    LEFT JOIN education ON employee.id = education.employee_id
    LEFT JOIN work_experience ON employee.id = work_experience.employee_id